﻿using Microsoft.AspNet.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cascomio.SecretSanta.Web.Middleware
{
    public static class RedirectMiddleware
    {
		public static IApplicationBuilder UseRedirects(this IApplicationBuilder app)
		{
			return app.Use(async (context, next) => {
				if (!context.Request.Path.StartsWithSegments("/") 
					&& !context.Request.Path.StartsWithSegments("/api")
					&& !context.Request.Path.StartsWithSegments("/images"))
				{
					context.Response.Redirect($"/#{context.Request.Path.Value.Substring(1)}", true);
				}
				else
				{
					await next();
				}
			});
		}
	}
}
