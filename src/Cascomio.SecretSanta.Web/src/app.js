export class App {
  configureRouter(config, router) {
    config.title = 'Secret Santa';
  	config.options.pushState = true;
    config.map([
      { route: ['', 'welcome'], name: 'welcome',      moduleId: 'welcome',      nav: true, title: 'Welcome' },
      { route: 'users',         name: 'users',        moduleId: 'users',        nav: true, title: 'Github Users' },
      { route: 'child-router',  name: 'child-router', moduleId: 'child-router', nav: true, title: 'Child Router' },
      { route: '/exchange/ongoing',	name: 'exchange-ongoing',        moduleId: './exchange/ongoing',        nav: true, title: 'Ongoing' },
      { route: '/exchange/done',  name: 'exchange-done', moduleId: './exchange/done', nav: true, title: 'Done' },
      { route: '/exchange/new',         name: 'exchange-new',        moduleId: './exchange/new',        nav: true, title: 'New' },
      { route: '/user/giftees',  name: 'user-giftees', moduleId: './user/giftees', nav: true, title: 'Giftees' },
      { route: '/user/invite',         name: 'user-invite',        moduleId: './user/invite',        nav: true, title: 'Invite' },
      { route: '/user/profile',  name: 'user-profile', moduleId: './user/profile', nav: true, title: 'Profile' }
    ]);

    this.router = router;
  }
}
