import {App} from '../../src/app';

class RouterStub {
  configure(handler) {
    handler(this);
  }

  map(routes) {
    this.routes = routes;
  }
}

describe('the App module', () => {
  var sut;
  var mockedRouter;

  beforeEach(() => {
    mockedRouter = new RouterStub();
    mockedRouter.options = {};
    sut = new App();
    sut.configureRouter(mockedRouter, mockedRouter);
  });

  it('contains a router property', () => {
    expect(sut.router).toBeDefined();
  });

  it('configures the router title', () => {
    expect(sut.router.title).toEqual('Secret Santa');
  });

  it('should have a welcome route', () => {
    expect(sut.router.routes).toContain({ route: ['', 'welcome'], name: 'welcome',  moduleId: 'welcome', nav: true, title: 'Welcome' });
  });

  it('should have a ongoing exchange route', () => {
    expect(sut.router.routes).toContain({ route: '/exchange/ongoing', name: 'exchange-ongoing', moduleId: './exchange/ongoing', nav: true, title: 'Ongoing' });
  });

  it('should have a done exchange route', () => {
    expect(sut.router.routes).toContain({ route: '/exchange/done', name: 'exchange-done', moduleId: './exchange/done', nav: true, title: 'Done' });
  });

  it('should have a new exchange route', () => {
    expect(sut.router.routes).toContain({ route: '/exchange/new', name: 'exchange-new', moduleId: './exchange/new', nav: true, title: 'New' });
  });

  it('should have a new exchange route', () => {
    expect(sut.router.routes).toContain({ route: '/exchange/new', name: 'exchange-new', moduleId: './exchange/new', nav: true, title: 'New' });
  });
});
